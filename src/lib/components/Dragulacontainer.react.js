import React, { useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import dragula from 'dragula';
import './assets/dragula.min.css';

const DragulaContainer = ({ children, setProps }) => {
    const containerRef = useRef();

    useEffect(() => {
        const drake = dragula([containerRef.current]);
        drake.on('dragend', () => {
            const newOrder = Array.from(containerRef.current.children).map(child => child.id);
            if (setProps) {
                setProps({ childrenOrder: newOrder });
            }
        });
        return () => {
            drake.destroy();
        };
    }, []);

    return (
        <div ref={containerRef}>
            {children}
        </div>
    );
};

DragulaContainer.propTypes = {
    id: PropTypes.string,
    children: PropTypes.node,
    setProps: PropTypes.func,
    childrenOrder: PropTypes.arrayOf(PropTypes.string),
};

export default DragulaContainer;