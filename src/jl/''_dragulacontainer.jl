# AUTO GENERATED FILE - DO NOT EDIT

export ''_dragulacontainer

"""
    ''_dragulacontainer(;kwargs...)
    ''_dragulacontainer(children::Any;kwargs...)
    ''_dragulacontainer(children_maker::Function;kwargs...)


A Dragulacontainer component.

Keyword arguments:
- `children` (a list of or a singular dash component, string or number; optional)
- `id` (String; optional)
- `childrenOrder` (Array of Strings; optional)
"""
function ''_dragulacontainer(; kwargs...)
        available_props = Symbol[:children, :id, :childrenOrder]
        wild_props = Symbol[]
        return Component("''_dragulacontainer", "Dragulacontainer", "dragulacontainer", available_props, wild_props; kwargs...)
end

''_dragulacontainer(children::Any; kwargs...) = ''_dragulacontainer(;kwargs..., children = children)
''_dragulacontainer(children_maker::Function; kwargs...) = ''_dragulacontainer(children_maker(); kwargs...)

